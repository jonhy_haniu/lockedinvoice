import android.content.Context
import android.view.ViewGroup

class GrupoTeclasNo() {


    val letras: MutableList<CharSequence> = mutableListOf<CharSequence>()
    var passos : MutableList<GrupoTeclasNoPasso> = mutableListOf()
    var passoAtualIndex : Int = -1


    fun passoAtual() : GrupoTeclasNoPasso{
        return passos[passoAtualIndex]
    }

    fun proximoPasso(){
        passoAtualIndex = (passoAtualIndex + 1) % passos.count()
    }

    fun resetPassoAtual() {
       passoAtualIndex = -1
    }


}


class GrupoTeclasNoPasso(botaoInicial : Int, botaoFinal : Int, proximoGrupo:GrupoTeclasNo, final : Boolean = false, val viewGroup : ViewGroup){
   val botaoInicial = botaoInicial
   val botaoFinal = botaoFinal
   val proximoGrupo = proximoGrupo
   val final = final


    fun proximoGrupo() : GrupoTeclasNo{
        proximoGrupo.resetPassoAtual()
        return proximoGrupo
    }

}