package br.com.daitek.lockedinvoice

import android.util.Log
import android.view.View
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions

public class FaceImageAnalyzer(val principalActivity: PrincipalActivity) : ImageAnalysis.Analyzer {


    var olhoFechado = false

    @androidx.camera.core.ExperimentalGetImage
    override fun analyze(imageProxy: ImageProxy) {


        val mediaImage = imageProxy.image

        if(mediaImage != null){
            val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)

            val realTimeOpts = FaceDetectorOptions.Builder()
                    .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                    .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
                    .setContourMode(FaceDetectorOptions.CONTOUR_MODE_NONE)
                    .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_NONE)
                    .build()
            val detector = FaceDetection.getClient(realTimeOpts)
            val result = detector.process(image)
                    .addOnSuccessListener { faces ->


                        for (face in faces) {

                            if( !olhoFechado ) {
                                if (face.rightEyeOpenProbability != null) {
                                    if(face.rightEyeOpenProbability < 0.1)
                                        olhoFechado = true
                                }
                            }
                            else {
                                if (face.rightEyeOpenProbability != null) {
                                    if(face.rightEyeOpenProbability > 0.9) {
                                        olhoFechado = false
                                        Log.d("CameraXBasic", "PISCOU $face.rightEyeOpenProbability")

                                        principalActivity.runOnUiThread(Runnable {
                                            principalActivity.piscada()
                                        })
                                    }
                                }
                            }



                        }


                        imageProxy.close()

                    }
                    .addOnFailureListener { e ->
                        Log.d("CameraXBasic", "ERRO NO PROCESSAMENTO DA IMAGEM - DETEÇÃO DE FACE")
                        imageProxy.close()
                    }

        }

    }

}