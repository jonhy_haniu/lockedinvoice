package br.com.daitek.lockedinvoice

import GrupoTeclasNo
import GrupoTeclasNoPasso
import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.View
import android.widget.GridLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageCapture
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_principal.*
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class PrincipalActivity : AppCompatActivity(), TextToSpeech.OnInitListener  {
    lateinit var root : GrupoTeclasNo
    lateinit var atual : GrupoTeclasNo
    lateinit var textToSpeech : TextToSpeech
    val intervalBetweenKeys = 1500L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)

// CAMERA ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                    this, PrincipalActivity.REQUIRED_PERMISSIONS, PrincipalActivity.REQUEST_CODE_PERMISSIONS)
        }

        cameraExecutor = Executors.newSingleThreadExecutor()

// FIM CAMERA ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        val keyboardGridLayout = findViewById<GridLayout>(R.id.keyboardGridLayout)
        val actionsLinearLayout = findViewById<LinearLayout>(R.id.actionsLinearLayout)


        // FOI PRECISO ATUALIZAR O GOOGLE SPEECH SERVICES PARA RESOLVER O PROBLEMA DO
        textToSpeech = TextToSpeech(this, this)



        val g1 =  GrupoTeclasNo()
        val g2 =  GrupoTeclasNo()
        val g3 =  GrupoTeclasNo()
        val g4 =  GrupoTeclasNo()
        val g5 =  GrupoTeclasNo()
        val g6 =  GrupoTeclasNo()


        root = GrupoTeclasNo()

        root.letras += mutableListOf<CharSequence>("a", "b", "c", "d", "e", "f", "g",
                "h", "i", "j", "k", "l", "m", "n",
                "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z", " ", " ",
                "0", "1", "2", "3", "4", " ", "9",
                ".", ",", "!", "?", " ", " ", " ")


        root.passos.add(GrupoTeclasNoPasso(0, 6, g1, false, keyboardGridLayout))
        root.passos.add(GrupoTeclasNoPasso(7, 13, g2, false, keyboardGridLayout))
        root.passos.add(GrupoTeclasNoPasso(14, 20, g3, false, keyboardGridLayout))
        root.passos.add(GrupoTeclasNoPasso(21, 27, g4, false, keyboardGridLayout))
        root.passos.add(GrupoTeclasNoPasso(28, 34, g5, false, keyboardGridLayout))
        root.passos.add(GrupoTeclasNoPasso(35, 41, g6, false, keyboardGridLayout))

        // actions
        val actionsList = mutableListOf<GrupoTeclasNoPasso>()

        for( i in 0..actionsLinearLayout.childCount-2){
            actionsList.add(GrupoTeclasNoPasso(i, i, root, true, actionsLinearLayout))
        }

        root.passos.addAll(actionsList)


        g1.letras += mutableListOf<CharSequence>("a", "b", "c", "d", "e", "f", "g")
        for(i in 0..g1.letras.count()-1){
            g1.passos.add(GrupoTeclasNoPasso(i, i, root, true, keyboardGridLayout))
        }

        g1.passos.addAll(actionsList)

        g2.letras += mutableListOf<CharSequence>("h", "i", "j", "k", "l", "m", "n")
        for(i in 0..g2.letras.count()-1){
            g2.passos.add(GrupoTeclasNoPasso(i, i, root, true, keyboardGridLayout))
        }

        g2.passos.addAll(actionsList)

        g3.letras += mutableListOf<CharSequence>("o", "p", "q", "r", "s", "t", "u")
        for(i in 0..g3.letras.count()-1){
            g3.passos.add(GrupoTeclasNoPasso(i, i, root, true, keyboardGridLayout))
        }

        g3.passos.addAll(actionsList)

        g4.letras += mutableListOf<CharSequence>("v", "w", "x", "y", "z")
        for(i in 0..g4.letras.count()-1){
            g4.passos.add(GrupoTeclasNoPasso(i, i, root, true, keyboardGridLayout))
        }

        g4.passos.addAll(actionsList)

        g5.letras += mutableListOf<CharSequence>("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")
        for(i in 0..g5.letras.count()-1){
            g5.passos.add(GrupoTeclasNoPasso(i, i, root, true, keyboardGridLayout))
        }

        g5.passos.addAll(actionsList)

        g6.letras += mutableListOf<CharSequence>(".", ",", "!", "?")
        for(i in 0..g6.letras.count()-1){
            g6.passos.add(GrupoTeclasNoPasso(i, i, root, true, keyboardGridLayout))
        }

        g6.passos.addAll(actionsList)



        atual = root

        Timer().scheduleAtFixedRate(object : TimerTask() {
            @RequiresApi(Build.VERSION_CODES.M)
            override fun run() {

                runOnUiThread(Runnable {
                    Log.d("LockedInVoice", "TICK")


                    // é provavel que o problema de estar ligado e desligando
                    // seja por uma falta de sincronia na chamada
                    // adicionar um semaforo para verificar se o processamento terminou
                    // antes de iniciar uma nova pintura

                    atual.proximoPasso()

                    // desativar o background de todos os botoes e limpar os textos
                    for (childIdx in 0 until keyboardGridLayout.childCount) {
                        val textView = keyboardGridLayout.getChildAt(childIdx) as TextView
                        textView.setBackgroundResource(R.drawable.keyboard_button_common_shape)
                        textView.text = ""
                    }

                    for (childIdx in 0 until actionsLinearLayout.childCount - 1) {
                        val textView = actionsLinearLayout.getChildAt(childIdx) as TextView
                        textView.setBackgroundResource(R.drawable.keyboard_button_backspace_shape)
                    }


                    for (i in 0 until atual.letras.count()) {
                        val textView = keyboardGridLayout.getChildAt(i) as TextView
                        textView.text = atual.letras[i]
                    }

                    // ILUMINAR OS BOTÕES

                    val passoAtual = atual.passoAtual()



                    for (i in passoAtual.botaoInicial..passoAtual.botaoFinal) {
                        val textView = passoAtual.viewGroup.getChildAt(i) as TextView
                        textView.setBackgroundResource(R.drawable.keyboard_button_common_active_shape)
                    }


                })

            }
        }, 0, intervalBetweenKeys)


    }

    fun piscada(){
        Log.d("CameraXBasic", "clicado")

        val passoAtual = atual.passoAtual()
        if(passoAtual.final){

            if(passoAtual.viewGroup == actionsLinearLayout){

                val textView = actionsLinearLayout.getChildAt(passoAtual.botaoInicial) as TextView
                val label = textView.text.toString()
                if(textView.text.equals("apagar"))
                    apagar_onclick(textView)
                else if(textView.text.equals("espaço"))
                    espaco_onclick(textView)
                else if(textView.text.toString() == "voltar")
                    voltar_onclick(textView)
                else if(textView.text.toString() == "falar")
                    falar_onclick(textView)

                return

            }

            // pequeno hack pq sei que as seleções finais só tem 1 letra
            val textView = keyboardGridLayout.getChildAt(passoAtual.botaoInicial) as TextView

            val sb = StringBuilder()
            sb.append(mensagemTextView.text)
            sb.append(textView.text)
            mensagemTextView.text = sb.toString()

            root.resetPassoAtual()
            atual = root


        }
        else{
            atual = atual.passoAtual().proximoGrupo
        }
    }

    fun mensagemTextView_click(view: View) {
        piscada()
    }

    fun voltar_onclick(view: View) {
        root.resetPassoAtual()
        atual = root
    }
    fun falar_onclick(view: View) {

        textToSpeech!!.speak(mensagemTextView.text, TextToSpeech.QUEUE_FLUSH, null,"")

    }
    fun apagar_onclick(view: View) {

        val tam = mensagemTextView.text.length

        if(tam == 0)
            return

        mensagemTextView.text = mensagemTextView.text.subSequence(0, tam - 1)

    }



    public override fun onDestroy() {
        // Shutdown TTS
        if (textToSpeech != null) {
            textToSpeech!!.stop()
            textToSpeech!!.shutdown()
        }
        super.onDestroy()
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {

            val result = textToSpeech.setLanguage(Locale("pt", "BR"));

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "The Language specified is not supported!")
                Toast.makeText(this, "Não foi possível carregar o idioma PT-BR", Toast.LENGTH_SHORT)
            } else {
                // button_speak.isEnabled = true
            }
        } else {
            Log.e("TTS", "Initialization Failed!")
            Toast.makeText(this, "Erro ao carregar o módulo TextToSpeech. Atualize o aplicativo Google Speech Service", Toast.LENGTH_SHORT)
        }
    }

    fun espaco_onclick(view: View) {
        mensagemTextView.text = mensagemTextView.text.toString() + " "
    }

    private fun checkBuildConfig(): Boolean {
        var isEmulator = (Build.MANUFACTURER.contains("Genymotion")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.toLowerCase().contains("droid4x")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.HARDWARE == "goldfish"
                || Build.HARDWARE == "vbox86"
                || Build.HARDWARE.toLowerCase().contains("nox")
                || Build.FINGERPRINT.startsWith("generic")
                || Build.PRODUCT == "sdk"
                || Build.PRODUCT == "google_sdk"
                || Build.PRODUCT == "sdk_x86"
                || Build.PRODUCT == "vbox86p"
                || Build.PRODUCT.toLowerCase().contains("nox")
                || Build.BOARD.toLowerCase().contains("nox")
                || (Build.BRAND.startsWith("generic") &&    Build.DEVICE.startsWith("generic")))
        return isEmulator
    }


    // CAMERA //////////////////////////////////////////////////////////////////////////////////////

    private var imageCapture: ImageCapture? = null
    private lateinit var cameraExecutor: ExecutorService

    companion object {
        private const val TAG = "LOCKEDIN_VOICE"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

    private fun allPermissionsGranted() = PrincipalActivity.REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
                baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun startCamera() {

        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider  : ProcessCameraProvider = cameraProviderFuture.get()
            val preview = Preview.Builder()
                    .build()
                    .also{
                        it.setSurfaceProvider(viewFinder.surfaceProvider)
                    }

            val cameraSelector : CameraSelector =  CameraSelector.DEFAULT_BACK_CAMERA

            /*
            if(checkBuildConfig()){
                cameraSelector  = CameraSelector.DEFAULT_BACK_CAMERA
            }
            else{
                cameraSelector  = CameraSelector.DEFAULT_FRONT_CAMERA
            }

            */



            try{

                imageCapture = ImageCapture.Builder().build()

                cameraProvider.unbindAll()

                val imageAnalysis = ImageAnalysis.Builder()
                        .build()
                        .also{
                            it.setAnalyzer(cameraExecutor, FaceImageAnalyzer(this))
                        }

                cameraProvider.bindToLifecycle(this, cameraSelector, preview)
                cameraProvider.bindToLifecycle(this, cameraSelector, imageAnalysis)
            }
            catch(exc: Exception ){
                Log.e(PrincipalActivity.TAG, "Ligação da camera com o ciclo de vida falhou", exc)
                Toast.makeText(this, "Ligação da camera com o ciclo de vida falhou", Toast.LENGTH_SHORT)

            }

        }, ContextCompat.getMainExecutor(this))

    }


    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>, grantResults:
            IntArray) {
        if (requestCode == PrincipalActivity.REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(this,
                        "Você precisa dar permissão de acesso a camera para usar este aplicativo.",
                        Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    // FIM CAMERA //////////////////////////////////////////////////////////////////////////////////////
}